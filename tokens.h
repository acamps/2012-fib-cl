#ifndef tokens_h
#define tokens_h
/* tokens.h -- List of labelled tokens and stuff
 *
 * Generated from: cl.g
 *
 * Terence Parr, Will Cohen, and Hank Dietz: 1989-2001
 * Purdue University Electrical Engineering
 * ANTLR Version 1.33MR33
 */
#define zzEOF_TOKEN 1
#define INPUTEND 1
#define PROGRAM 2
#define ENDPROGRAM 3
#define VARS 4
#define ENDVARS 5
#define INT 6
#define BOOL 7
#define STRUCT 8
#define ENDSTRUCT 9
#define WRITELN 10
#define WRITE 11
#define READ 12
#define REF 13
#define VAL 14
#define PROCEDURE 15
#define ENDPROCEDURE 16
#define FUNCTION 17
#define RETURN 18
#define ENDFUNCTION 19
#define ARRAY 20
#define TRU 21
#define FALS 22
#define AND 23
#define OR 24
#define OF 25
#define NOT 26
#define IFF 27
#define THEN 28
#define ELSE 29
#define ENDIF 30
#define WHILE 31
#define DO 32
#define ENDWHILE 33
#define PLUS 34
#define MINUS 35
#define DIV 36
#define MULTI 37
#define OPENPAR 38
#define CLOSEPAR 39
#define OPENCLAU 40
#define CLOSECLAU 41
#define BIGGER 42
#define SMALLER 43
#define ASIG 44
#define EQUAL 45
#define DOT 46
#define COMA 47
#define QUOTES 48
#define IDENT 49
#define STRING 50
#define INTCONST 51
#define COMMENT 52
#define WHITESPACE 53
#define NEWLINE 54
#define LEXICALERROR 55

#ifdef __USE_PROTOS
void program(AST**_root);
#else
extern void program();
#endif

#ifdef __USE_PROTOS
void dec_vars(AST**_root);
#else
extern void dec_vars();
#endif

#ifdef __USE_PROTOS
void l_dec_vars(AST**_root);
#else
extern void l_dec_vars();
#endif

#ifdef __USE_PROTOS
void dec_var(AST**_root);
#else
extern void dec_var();
#endif

#ifdef __USE_PROTOS
void l_dec_blocs(AST**_root);
#else
extern void l_dec_blocs();
#endif

#ifdef __USE_PROTOS
void dec_bloc(AST**_root);
#else
extern void dec_bloc();
#endif

#ifdef __USE_PROTOS
void proc_header(AST**_root);
#else
extern void proc_header();
#endif

#ifdef __USE_PROTOS
void funct_header(AST**_root);
#else
extern void funct_header();
#endif

#ifdef __USE_PROTOS
void funct_return(AST**_root);
#else
extern void funct_return();
#endif

#ifdef __USE_PROTOS
void params(AST**_root);
#else
extern void params();
#endif

#ifdef __USE_PROTOS
void proc_param(AST**_root);
#else
extern void proc_param();
#endif

#ifdef __USE_PROTOS
void constr_type(AST**_root);
#else
extern void constr_type();
#endif

#ifdef __USE_PROTOS
void field(AST**_root);
#else
extern void field();
#endif

#ifdef __USE_PROTOS
void l_instrs(AST**_root);
#else
extern void l_instrs();
#endif

#ifdef __USE_PROTOS
void instruction(AST**_root);
#else
extern void instruction();
#endif

#ifdef __USE_PROTOS
void funct_inter(AST**_root);
#else
extern void funct_inter();
#endif

#ifdef __USE_PROTOS
void assign(AST**_root);
#else
extern void assign();
#endif

#ifdef __USE_PROTOS
void myif(AST**_root);
#else
extern void myif();
#endif

#ifdef __USE_PROTOS
void mywhile(AST**_root);
#else
extern void mywhile();
#endif

#ifdef __USE_PROTOS
void binary_expr(AST**_root);
#else
extern void binary_expr();
#endif

#ifdef __USE_PROTOS
void bool_expr(AST**_root);
#else
extern void bool_expr();
#endif

#ifdef __USE_PROTOS
void expression(AST**_root);
#else
extern void expression();
#endif

#ifdef __USE_PROTOS
void expr(AST**_root);
#else
extern void expr();
#endif

#ifdef __USE_PROTOS
void expsimple(AST**_root);
#else
extern void expsimple();
#endif

#endif
extern SetWordType zzerr1[];
extern SetWordType zzerr2[];
extern SetWordType setwd1[];
extern SetWordType zzerr3[];
extern SetWordType zzerr4[];
extern SetWordType zzerr5[];
extern SetWordType setwd2[];
extern SetWordType zzerr6[];
extern SetWordType zzerr7[];
extern SetWordType zzerr8[];
extern SetWordType zzerr9[];
extern SetWordType setwd3[];
extern SetWordType zzerr10[];
extern SetWordType zzerr11[];
extern SetWordType setwd4[];
extern SetWordType zzerr12[];
extern SetWordType zzerr13[];
extern SetWordType zzerr14[];
extern SetWordType zzerr15[];
extern SetWordType setwd5[];
extern SetWordType zzerr16[];
extern SetWordType zzerr17[];
extern SetWordType setwd6[];
