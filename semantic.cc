/* ===== semantic.c ===== */
#include <string>
#include <iostream>
#include <map>
#include <list>
#include <vector>


using namespace std;

#include <stdio.h>
#include <stdlib.h>
#include "ptype.hh"
#include "symtab.hh"

#include "myASTnode.hh"

#include "semantic.hh"

// feedback the main program with our error status
int TypeError = 0;


/// ---------- Error reporting routines --------------

void errornumparam(int l) {
  TypeError = 1;
  cout<<"L. "<<l<<": The number of parameters in the call do not match."<<endl;
}

void errorincompatibleparam(int l,int n) {
  TypeError = 1;
  cout<<"L. "<<l<<": Parameter "<<n<<" with incompatible types."<<endl;
}

void errorreferenceableparam(int l,int n) {
  TypeError = 1;
  cout<<"L. "<<l<<": Parameter "<<n<<" is expected to be referenceable but it is not."<<endl;
}

void errordeclaredident(int l, string s) {
  TypeError = 1;
  cout<<"L. "<<l<<": Identifier "<<s<<" already declared."<<endl;
}

void errornondeclaredident(int l, string s) {
  TypeError = 1;
  cout<<"L. "<<l<<": Identifier "<<s<<" is undeclared."<<endl;
}

void errornonreferenceableleft(int l, string s) {
  TypeError = 1;
  cout<<"L. "<<l<<": Left expression of assignment is not referenceable."<<endl;
}

void errorincompatibleassignment(int l) {
  TypeError = 1;
  cout<<"L. "<<l<<": Assignment with incompatible types."<<endl;
}

void errorbooleanrequired(int l,string s) {
  TypeError = 1;
  cout<<"L. "<<l<<": Instruction "<<s<<" requires a boolean condition."<<endl;
}

void errorisnotprocedure(int l) {
  TypeError = 1;
  cout<<"L. "<<l<<": Operator ( must be applied to a procedure in an instruction."<<endl;
}

void errorisnotfunction(int l) {
  TypeError = 1;
  cout<<"L. "<<l<<": Operator ( must be applied to a function in an expression."<<endl;
}

void errorincompatibleoperator(int l, string s) {
  TypeError = 1;
  cout<<"L. "<<l<<": Operator "<<s<<" with incompatible types."<<endl;
}

void errorincompatiblereturn(int l) {
  TypeError = 1;
  cout<<"L. "<<l<<": Return with incompatible type."<<endl;
}

void errorreadwriterequirebasic(int l, string s) {
  TypeError = 1;
  cout<<"L. "<<l<<": Basic type required in "<<s<<"."<<endl;
}

void errornonreferenceableexpression(int l, string s) {
  TypeError = 1;
  cout<<"L. "<<l<<": Referenceable expression required in "<<s<<"."<<endl;
}

void errornonfielddefined(int l, string s) {
  TypeError = 1;
  cout<<"L. "<<l<<": Field "<<s<<" is not defined in the struct."<<endl;
}

void errorfielddefined(int l, string s) {
  TypeError = 1;
  cout<<"L. "<<l<<": Field "<<s<<" already defined in the struct."<<endl;
}

/// ------------------------------------------------------------
/// Table to store information about program identifiers
symtab symboltable;

static void InsertintoST(int line,string kind,string id,ptype tp)
{
  infosym p;

  if (symboltable.find(id) && symboltable.jumped_scopes(id)==0) errordeclaredident(line,id);
  else {
    symboltable.createsymbol(id);
    symboltable[id].kind=kind;
    symboltable[id].tp=tp;
  }
}

/// ------------------------------------------------------------

bool isbasickind(string kind) {
  return kind=="int" || kind=="bool" || kind=="string";
}


bool check_numparams(AST *a, ptype tp) {
  while(a!=0 && tp!=0) {
    a = a->right;
    tp = tp->right;
  }
  return (a==0 && tp ==0);
}

void check_params(AST *a,ptype tp,int line,int numparam)
{
  if(!a) return;
  //if(!tp) return;
  //cerr << "c_p entering " << endl;
  TypeCheck(a);
  //cerr << "c_p tc done " << tp->kind << endl;
  if(tp->kind =="parref" && !a->ref) { //hay un caso que llega sin tp
    errorreferenceableparam(line,numparam);
  }
  //cerr << "c_p first cond checked"<< endl;
  if(a->tp->kind != "error" && !equivalent_types(a->tp, tp->down)){
    errorincompatibleparam(line,numparam);
  }
  //cerr << "c_p before recursive " << endl;
  check_params(a->right,tp->right,line,numparam+1);
}

void insert_params(AST *a)
{
  if(!a) return;
  InsertintoST(a->line,"idpar"+a->kind, child(a,0)->text, child(a,1)->tp);
  insert_params(a->right);
}

void insert_vars(AST *a)
{
  if (!a) return;
  TypeCheck(child(a,0));
  InsertintoST(a->line,"idvarlocal",a->text,child(a,0)->tp);
  insert_vars(a->right); 
}

void construct_struct(AST *a)
{
  AST *a1=child(a,0);
  a->tp=create_type("struct",0,0);

  while (a1!=0) {
    TypeCheck(child(a1,0));
    if (a->tp->struct_field.find(a1->text)==a->tp->struct_field.end()) {
      a->tp->struct_field[a1->text]=child(a1,0)->tp;
      a->tp->ids.push_back(a1->text);
     } else {
      errorfielddefined(a1->line,a1->text);
    }
    a1=a1->right;
  }
}

void construct_array(AST *a) {
  a->tp=create_type("array",0,0);
  TypeCheck(child(a,0));
  a->tp->numelemsarray = atoi(child(a,0)->text.c_str());
  TypeCheck(child(a,1));
  a->tp->down = child(a,1)->tp;
}

void create_header(AST *a)
{
  //cerr << "llego a create header con "<< a->kind << endl; //a solo dice procedure, interesan los hijos.
  if(a->kind == "procedure") a->tp = create_type("procedure",0,0);
  else if(a->kind == "function") a->tp = create_type("function",0,0);
  a->tp->down = create_type("list",0,0);
  AST* param = child(child(child(a,0),0),0); //El hijo dice el nombre de la funcion, los parametros no estan hasta el siguiente nivel.
  //esta funcion debe crear la cabezera y guardar la informacion sobre sus params.
  ptype ptparam;
  ptype previous = a->tp->down;
  bool first = true;
  while(param != 0) {
    //cerr << "bucle " <<endl;
    //cerr << param->kind << endl;
    if (param->kind == "ref") ptparam = create_type("parref",0,0);
    else if (param->kind == "val") ptparam = create_type("parval",0,0);
    TypeCheck(child(param,1));
    ptparam->down = child(param,1)->tp;
    if(first) {
      previous->down = ptparam;
      first = false;
    } else {
      previous->right = ptparam;
    }
    previous = ptparam;
    param = param->right;
  }
  if (a->kind == "function") {
    TypeCheck(child(child(a,0),1));
    a->tp->right = child(child(a,0),1)->tp;
  }
}

void check_type(AST *a) {
  
  if(child(a,0)->tp->down!=0) {
    if (!check_numparams(child(child(a,1),0), child(a,0)->tp->down->down)) errornumparam(a->line);
    else
    check_params(child(child(a,1),0), child(a,0)->tp->down->down, a->line,1);
  }
}

void insert_header(AST *a)
{
  if (!a) return;
  //cerr << "i want to create" << endl;
  create_header(a);
  //cerr << "i created, and I want to insert" << endl;
  if(a->kind=="procedure") InsertintoST(a->line,"idproc",child(a,0)->text,a->tp);
  else if(a->kind=="function") InsertintoST(a->line,"idfunc",child(a,0)->text,a->tp);
  //cerr << "inserted";
  //cerr << "llego aquí"<<child(a,0)->text << endl;
}

void insert_headers(AST *a)
{
  while (a!=0) {
    //cerr << "here i go" << endl;
    insert_header(a);
    a=a->right;
  }
}


void TypeCheck(AST *a,string info)
{
  //cerr << "DIEEEE " << a->kind <<endl ;
  if (!a) {
    return;
  }

  //cout<<"Starting with node \""<<a->kind<<"\""<<endl;
  if (a->kind=="program") {
    a->sc=symboltable.push();
    insert_vars(child(child(a,0),0));
    //cerr << "vars inserted" <<endl;
    insert_headers(child(child(a,1),0));
    //cerr << "headers inserted" << endl;
    TypeCheck(child(a,1));
    TypeCheck(child(a,2),"instruction");

    symboltable.pop();
  }
  else if (a->kind=="list") {
    // At this point only instruction, procedures or parameters lists are possible.
    for (AST *a1=a->down;a1!=0;a1=a1->right) {
      TypeCheck(a1,info);
    }
  }
  else if (a->kind=="if" || a->kind == "while") {
    TypeCheck(child(a,0));
    //cerr <<"iffff itself "  << child(a,0)->tp->kind << endl;
    if(child(a,0)->tp->kind!= "error" && child(a,0)->tp->kind!="bool") {
      errorbooleanrequired(a->line,a->kind);
    }
    TypeCheck(child(a,1),"instruction");
    TypeCheck(child(a,2),"instruction");
  }
  else if (a->kind=="ident") {
    //cerr<< "el texto de la identidad es " << a->text << endl;
    if (!symboltable.find(a->text)) {
      errornondeclaredident(a->line, a->text);
    } 
    else {
      a->tp=symboltable[a->text].tp;
      if(a->tp->kind != "procedure" && a->tp->kind != "function") a->ref=1;
    }
    //cerr << "salgo de ident vivo" <<endl;
  }
  else if (a->kind=="struct") {
    construct_struct(a);
  }
  else if (a->kind=="array") {
    construct_array(a);
  }
  else if (a->kind==":=") {
    TypeCheck(child(a,0));
    TypeCheck(child(a,1));
    if (!child(a,0)->ref) {
      errornonreferenceableleft(a->line,child(a,0)->text);
    }
    else if (child(a,0)->tp->kind!="error" && child(a,1)->tp->kind!="error" &&
	     !equivalent_types(child(a,0)->tp,child(a,1)->tp)) {
      errorincompatibleassignment(a->line);
    } 
    else {
      a->tp=child(a,0)->tp;
    }
  }
  else if (a->kind=="intconst") {
    a->tp=create_type("int",0,0);
  }
  else if ((a->kind=="true") || (a->kind=="false")) {
    a->tp=create_type("bool",0,0);
  }
  else if (a->kind=="[") {
    TypeCheck(child(a,0));
    TypeCheck(child(a,1));
    if ((child(a,0)->tp->kind!="error" && child(a,0)->tp->kind!="array")) {
      a->kind="array[]";
      errorincompatibleoperator(a->line,a->kind);
    }
    if ((child(a,1)->tp->kind!="error" && child(a,1)->tp->kind!="int")) {
      a->kind="[]";
      errorincompatibleoperator(a->line,a->kind);
    }
    if(child(a,0)->tp->kind =="array")a->tp=child(a,0)->tp->down;
    a->ref=child(a,0)->ref;
  }
  else if (a->kind==">" || a->kind=="<") {
    TypeCheck(child(a,0));
    TypeCheck(child(a,1));
    if ((child(a,0)->tp->kind!="error" && child(a,0)->tp->kind!="int") ||
	(child(a,1)->tp->kind!="error" && child(a,1)->tp->kind!="int")) {
      errorincompatibleoperator(a->line,a->kind);
    }
    a->tp=create_type("bool",0,0);
  }
  else if (a->kind=="and" || a->kind=="or") {
    TypeCheck(child(a,0));
    TypeCheck(child(a,1));
    bool cond1 = (child(a,0)->tp->kind!="error" && child(a,0)->tp->kind!="bool") || (child(a,1)->tp->kind!="error" && child(a,1)->tp->kind!="bool");
    if (cond1) {
      errorincompatibleoperator(a->line,a->kind);
    }
    a->tp=create_type("bool",0,0);
  }
  else if (a->kind=="not") {
    TypeCheck(child(a,0));
    if( (child(a,0)->tp->kind!="error" && child(a,0)->tp->kind!="bool")) {
      errorincompatibleoperator(a->line,a->kind);
    }
    a->tp=create_type("bool",0,0);
  }
  else if (a->kind=="=") {
    TypeCheck(child(a,0));
    TypeCheck(child(a,1));
    
    /*if (child(a,0)->tp->kind != "error" and child(a,1)->tp->kind != "error" and
      (child(a,0)->tp->kind != child(a,1)->tp->kind or (child(a,0)->tp->kind != "bool" and child(a,0)->tp->kind != "int"))) {
      errorincompatibleoperator(a->line, a->kind);
    }*/
    bool cond1 = (child(a,0)->tp->kind!="error" && child(a,0)->tp->kind=="int") && (child(a,1)->tp->kind!="error" && child(a,1)->tp->kind=="int");
    bool cond2 = (child(a,0)->tp->kind!="error" && child(a,0)->tp->kind=="bool") && (child(a,1)->tp->kind!="error" && child(a,1)->tp->kind=="bool");
    if (!(cond1 || cond2)) {
      errorincompatibleoperator(a->line,a->kind);
    }
    a->tp=create_type("bool",0,0);
  }
  else if ((a->kind=="+") || (a->kind=="-" && child(a,1)!=0) || (a->kind=="*")
	   || (a->kind=="/")) {
    TypeCheck(child(a,0));
    TypeCheck(child(a,1));
    if ((child(a,0)->tp->kind!="error" && child(a,0)->tp->kind!="int") ||
	(child(a,1)->tp->kind!="error" && child(a,1)->tp->kind!="int")) {
      errorincompatibleoperator(a->line,a->kind);
    }
    a->tp=create_type("int",0,0);
  }
  else if(a->kind=="-") {
    TypeCheck(child(a,0));
    if ((child(a,0)->tp->kind!="error" && child(a,0)->tp->kind!="int")) {
      errorincompatibleoperator(a->line,a->kind);
    }
    a->tp=create_type("int",0,0);
  }
  else if (isbasickind(a->kind)) {
    a->tp=create_type(a->kind,0,0);
  }
  else if (a->kind=="writeln") {
    TypeCheck(child(a,0));
    if (child(a,0)->tp->kind!="error" && !isbasickind(child(a,0)->tp->kind)) {
      errorreadwriterequirebasic(a->line,a->kind);
    }
  }
  else if (a->kind==".") {
    TypeCheck(child(a,0));
    a->ref=child(a,0)->ref;
    if (child(a,0)->tp->kind!="error") {
      if (child(a,0)->tp->kind!="struct") {
	errorincompatibleoperator(a->line,"struct.");
      }
      else if (child(a,0)->tp->struct_field.find(child(a,1)->text)==
	       child(a,0)->tp->struct_field.end()){
	/*if(child(a,0)->kind=="array") {
	    AST *a1=child(a,0);
	    if (child(a1,0)->tp->struct_field.find(child(a1,1)->text)==
	       child(a1,0)->tp->struct_field.end()) {*/
		 //errornonfielddefined(a1->line,child(a1,1)->text);
		 //errornonfielddefined(a->line,child(a,1)->text);
	  //    }
	//} else {
	errornonfielddefined(a->line,child(a,1)->text);
	//} 
      } 
      else {
	a->tp=child(a,0)->tp->struct_field[child(a,1)->text];
      }
    }
  }
  /*else if (a->kind =="write") {
    
  }*/
  else if (a->kind == "read" || a->kind =="write") {
    TypeCheck(child(a,0));
    if(child(a,0)->tp->kind != "error" && !child(a,0)->ref) {
      if(a->kind=="read") {
      errornonreferenceableexpression(a->line,a->kind);
      }
     }else if (child(a,0)->tp->kind != "error" && !isbasickind(child(a,0)->tp->kind)) {
      errorreadwriterequirebasic(a->line,a->kind);
    }
  }
  else if (a->kind =="procedure"|| a->kind == "function") {
    a->sc=symboltable.push();
    //cerr << "procedure start" <<endl;
    insert_params(child(child(child(a,0),0),0));
    //cerr << "procedure params inserted" <<endl;
    insert_vars(child(child(a,1),0));
    //cerr << "procedure vars inserted" <<endl;
    insert_headers(child(child(a,2),0));
    //cerr << "procedure headers inserted" <<endl;
    TypeCheck(child(a,2));
    TypeCheck(child(a,3),"instruction");
    if(a->kind == "function") {
      //... do some black magic.
      TypeCheck(child(a,4));//el return type, lo checkeamos tb.
      if(!equivalent_types(a->tp->right, child(a,4)->tp)) {
	errorincompatiblereturn(child(a,4)->line);
      }
    }

    symboltable.pop();
  }
  else if (a->kind == "(") {
    //cerr << "kind (" <<endl;
    TypeCheck(child(a,0)); //Hace falta poner esto, o falla por todas partes, no se acuerda de que debe mirar.
    if(info == "instruction") {
      if(child(a,0)->tp->kind != "error" && child(a,0)->tp->kind !="procedure") {
	errorisnotprocedure(a->line);
      }
      
    } else {
      if(child(a,0)->tp->kind != "error" && child(a,0)->tp->kind !="function") {
	errorisnotfunction(a->line);
      }
      else if (child(a,0)->tp->kind != "error") a->tp = child(a,0)->tp->right;
    }
    if(child(a,0)->tp->kind=="procedure" || child(a,0)->tp->kind=="function") {
      //cerr<<" entering into procedure" << endl;
      
      check_type(a);//child(child(a,1),0), child(a,0)->tp->down->down, a->line,1
    }
    //cerr << "end kind (" << endl;
  }
  else {
    //if ((a->kind=="true") || (a->kind=="false")) 
      cerr <<" no tiene sentidooo" <<a->kind << endl;
    cout<<"BIG PROBLEM! No case defined for kind "<<a->kind<<endl;
    cout << "aixo no te cap sentit" << endl;
  }

  //cout<<"Ending with node \""<<a->kind<<"\""<<endl;
}
